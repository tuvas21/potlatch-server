package com.kd7uiy.potlatch;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.kd7uiy.potlatch.repository.GiftChainRepository;
import com.kd7uiy.potlatch.repository.GiftRepository;
import com.kd7uiy.potlatch.repository.UserRepository;
import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftChain;
import com.kd7uiy.potlatch.shared.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {
	
	@Autowired
	UserRepository mUserRepository;
	
	@Autowired
	GiftRepository mGiftRepository;
	
	
	@Autowired
	GiftChainRepository mGiftChainRepository;
	
	
	//Lists by the most touching users
	//TODO This should really be a single query, however, this will work for now...
	@RequestMapping(value="/user", method=RequestMethod.GET) 
	public @ResponseBody List<User> getUser(){
		List<User> users=mUserRepository.findAll();
		List<User> ret=new ArrayList<>(users.size());
		for (User user: users) {
			int index=0;
			user.setGiftCount(mGiftRepository.getCountByUserId(user.getId()));
			user.setTouchCount(mGiftRepository.getTouchCountByUserId(user.getId()));
			user.setFlagCount(mGiftRepository.getFlagCountByUserId(user.getId()));
			
			for (User tuser: ret) {
				if (tuser.getTouchCount()<user.getTouchCount()) {
					break;
				}
				index++;
			}
			ret.add(index,user);
		}
		return ret;
	}
	
	@RequestMapping(value="/user/{id}/gifts",method=RequestMethod.GET) 
	public @ResponseBody List<Gift> getUserGifts(@PathVariable("id") long id){
		return mGiftRepository.findByUserId(id);
	}
	
	@RequestMapping(value="/user/{id}/chains",method=RequestMethod.GET) 
	public @ResponseBody List<GiftChain> getUserChains(@PathVariable("id") long id){
		return mGiftChainRepository.findByUserId(id);
	}
	
	@RequestMapping(value="/user", method=RequestMethod.POST) 
	@PreAuthorize("hasRole('write')")
	public @ResponseBody User getUser(@AuthenticationPrincipal User user){
		return user;
	}

	@RequestMapping(value="/user/new",method=RequestMethod.POST)
	public @ResponseBody User addUser(@RequestBody User newUser, HttpServletResponse response) {
		newUser.setId(0);	//Ensures that the next available Id is used
		if(mUserRepository.findByOpenId(newUser.getOpenId())==null)
		{
			newUser=mUserRepository.save(newUser);
//			System.out.println("New User="+newUser);
		} else {
			newUser=null;
			response.setStatus(406);
		}
		return newUser;
	}
	
	@RequestMapping(value="/user/update",method=RequestMethod.POST)
	@PreAuthorize("hasRole('write')")
	public @ResponseBody User editUser(@RequestBody User newUser,
			@AuthenticationPrincipal User user, HttpServletResponse response) {
//		System.out.println("Authenticated User="+user);

		if (user.getId()==newUser.getId()) {
			newUser=mUserRepository.save(newUser);
//			System.out.println("New User="+newUser);
		} else {
			response.setStatus(400);
		}
		return newUser;
	}
}
