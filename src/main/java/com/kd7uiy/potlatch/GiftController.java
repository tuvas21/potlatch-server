package com.kd7uiy.potlatch;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.kd7uiy.library.ResourceNotFoundException;
import com.kd7uiy.library.WrongUserFoundException;
import com.kd7uiy.potlatch.client.GiftSvcApi;
import com.kd7uiy.potlatch.repository.GiftChainRepository;
import com.kd7uiy.potlatch.repository.GiftImageRepository;
import com.kd7uiy.potlatch.repository.GiftRepository;
import com.kd7uiy.potlatch.repository.GiftVoteRepository;
import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftChain;
import com.kd7uiy.potlatch.shared.GiftImage;
import com.kd7uiy.potlatch.shared.GiftVote;
import com.kd7uiy.potlatch.shared.GiftVote.VoteType;
import com.kd7uiy.potlatch.shared.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Controller
public class GiftController{
	
	@Autowired
	GiftRepository mGiftRepository;
	
	@Autowired
	private GiftImageRepository mGiftImageRepository;

	@Autowired
	private GiftChainRepository mGiftChainRepository;
	
	@Autowired
	private GiftVoteRepository mGiftVoteRepository;
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH,method=RequestMethod.GET)
    public @ResponseBody Iterable<Gift> getGiftList() {
		return mGiftRepository.findAll();
	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/create",method=RequestMethod.POST)
	@PreAuthorize("hasRole('write')")
	public @ResponseBody Gift addGift(@RequestBody Gift v, @AuthenticationPrincipal User user) throws IOException {
//		System.out.println("Setting gift with user "+user);
		
		v.setUserId(user.getId());
		v.setTouchCount(0);
		v.setTouchedUsers(new ArrayList<GiftVote>());
		v.setFlagCount(0);
		v.setFlaggedUsers(new ArrayList<GiftVote>());
		
		v=mGiftRepository.save(v);
		return v;
	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/{id}/upload",method=RequestMethod.POST)
	@PreAuthorize("hasRole('write')")
	public @ResponseBody boolean uploadGiftImage(@PathVariable("id") long id,@RequestParam("photo") MultipartFile data,@AuthenticationPrincipal User user) throws IOException {
//		System.out.println("Starting in function uploadGiftImage");
		
		Gift gift=mGiftRepository.findOne(id);
//		System.out.println("Gift="+gift);
		
		
		if (gift==null) {
			throw new ResourceNotFoundException();
		}
		else if (gift.getUserId()!=user.getId()) {
			System.out.println("Authenticated user="+user);
			System.out.println("Gift User="+gift.getUserId());
			
			throw new WrongUserFoundException("Unauthorized User to upload image "+id);
		}
		
//		System.out.println("Checks complete");
		GiftImage image=new GiftImage(id,data.getBytes());
		
//		System.out.println("Ready to save");
		mGiftImageRepository.save(image);
		
		return true;
	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/{id}",
			method=RequestMethod.GET,
			produces=MediaType.IMAGE_PNG_VALUE)
	public @ResponseBody Gift getGiftInfo(@PathVariable("id") long id) throws IOException {
		return mGiftRepository.findOne(id);
	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/{id}/get",method=RequestMethod.GET)
	public @ResponseBody byte[] downloadGift(@PathVariable("id") long id) throws IOException {
		return mGiftImageRepository.findOne(id).getImage();
	}

	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/chain/{id}",method=RequestMethod.GET)
	public Collection<Gift> getGiftList(@PathVariable("chainId") long id) {
//		return null;	//TODO replace this
		return mGiftRepository.findByChainId(id);
	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/{id}/touch",method=RequestMethod.POST)
	@PreAuthorize("hasRole('write')")
	public @ResponseBody boolean touchGift(@PathVariable("id") long id,
			@AuthenticationPrincipal User user, HttpServletResponse response) {
		if (mGiftRepository.exists(id)) {
			Gift v=mGiftRepository.findOne(id);
			boolean contains=false;
			List<GiftVote> touchedUsers=v.getTouchedUsers();
			for (GiftVote vote :touchedUsers) {
				long tuserId=vote.getUserId();
				if (tuserId==user.getId()) {
					contains=true;
					break;
				}
			}
			if (!contains) {
				v.setTouchCount(v.getTouchCount()+1);
				GiftVote vote=new GiftVote(user,VoteType.TOUCHED,id);
				touchedUsers.add(vote);
				mGiftVoteRepository.save(vote);
				v.setTouchedUsers(touchedUsers);
				v = mGiftRepository.save(v);
//				System.out.println(v);
				response.setStatus(200);
				return true;
			} else {
				response.setStatus(400);
				return false;
			}
		} else {
			throw new ResourceNotFoundException();
		}
	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/{id}/untouch",method=RequestMethod.POST)
	@PreAuthorize("hasRole('write')")
	public @ResponseBody boolean untouchGift(@PathVariable("id") long giftId,
			@AuthenticationPrincipal User user, HttpServletResponse response) {
		System.out.println("Untouching gift "+giftId);
		if (mGiftRepository.exists(giftId)) {
			Gift v=mGiftRepository.findOne(giftId);
			List<GiftVote> touchedUsers=v.getTouchedUsers();
			GiftVote userVote=null;
			for (GiftVote vote :touchedUsers) {
				long tuserId=vote.getUserId();
				if (tuserId==user.getId()) {
					userVote=vote;
				}
			}
			if (userVote!=null) {
				v.setTouchCount(v.getTouchCount()-1);
				touchedUsers.remove(userVote);
				mGiftVoteRepository.delete(userVote);
				v.setTouchedUsers(touchedUsers);
				v = mGiftRepository.save(v);
				response.setStatus(200);
				return true;
			} else {
				response.setStatus(400);
				return false;
			}
		} else {
			throw new ResourceNotFoundException();
		}
	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/{id}/flag",method=RequestMethod.POST)
	@PreAuthorize("hasRole('write')")
	public @ResponseBody boolean flagGift(@PathVariable("id") long id,
			@AuthenticationPrincipal User user, HttpServletResponse response){
		if (mGiftRepository.exists(id)) {
			Gift v=mGiftRepository.findOne(id);
			boolean contains=false;
			List<GiftVote> flaggedUsers=v.getFlaggedUsers();
			for (GiftVote vote :flaggedUsers) {
				long tuserId=vote.getUserId();
				if (tuserId==user.getId()) {
					contains=true;
					break;
				}
			}
			if (!contains) {
				v.setFlagCount(v.getFlagCount()+1);
				GiftVote vote=new GiftVote(user,VoteType.FLAGGED,id);
				flaggedUsers.add(vote);
				mGiftVoteRepository.save(vote);
				v.setFlaggedUsers(flaggedUsers);
				v = mGiftRepository.save(v);
				response.setStatus(200);
				return true;
			} else {
				response.setStatus(400);
				return false;
			}
		} else {
			throw new ResourceNotFoundException();
		}	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/{id}/unflag",method=RequestMethod.POST)
	@PreAuthorize("hasRole('write')")
	public @ResponseBody boolean unflagGift(@PathVariable("id") long giftId,
			@AuthenticationPrincipal User user, HttpServletResponse response) {
		if (mGiftRepository.exists(giftId)) {
			Gift v=mGiftRepository.findOne(giftId);
			List<GiftVote> flaggedUsers=v.getFlaggedUsers();
			GiftVote userVote=null;
			for (GiftVote vote :flaggedUsers) {
				long tuserId=vote.getUserId();
				if (tuserId==user.getId()) {
					userVote=vote;
				}
			}
			if (userVote!=null) {
				v.setFlagCount(v.getFlagCount()-1);
				flaggedUsers.remove(userVote);
				mGiftVoteRepository.delete(userVote);
				v.setFlaggedUsers(flaggedUsers);
				v = mGiftRepository.save(v);
				response.setStatus(200);
				return true;
			} else {
				response.setStatus(400);
				return false;
			}
		} else {
			throw new ResourceNotFoundException();
		}
	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/{id}/chain",method=RequestMethod.POST)
	@PreAuthorize("hasRole('write')")
	public void setGiftChain(@PathVariable("id") long giftId,long chainId,
			@AuthenticationPrincipal User user) {
		Gift gift=mGiftRepository.findOne(giftId);
		if (gift.getUserId()!=user.getId()) {
			throw new UnauthorizedUserException("Can only add a gift to a chain if you own it!");
		}
		GiftChain chain=mGiftChainRepository.findOne(chainId);
		if (chain==null || gift==null) {
			throw new ResourceNotFoundException();
		}
		gift.setChainId(chainId);
		mGiftRepository.save(gift);
	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_TITLE_SEARCH_PATH,method=RequestMethod.GET)
	public @ResponseBody Collection<Gift> findByTitle(@PathVariable("title") String title) {
		return mGiftRepository.findByTitleContaining(title);
	}
	
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/{id}/touchedby",method=RequestMethod.GET)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	public @ResponseBody Collection<User> getUsersWhoGiftTouched(@PathVariable("id") long giftId) {
		return mGiftVoteRepository.findUserByGiftId(giftId);
	}
	
	//Most touching images
	@RequestMapping(value=GiftSvcApi.GIFT_SVC_PATH+"/touching",method=RequestMethod.GET)
	public @ResponseBody Collection<Gift> getMostTouchingGifts() {
		Sort sort=new Sort(Sort.Direction.DESC,new String[]{"touchCount"});
		return mGiftRepository.findAll(sort);
	}
}
