package com.kd7uiy.potlatch;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.kd7uiy.library.WrongUserFoundException;
import com.kd7uiy.potlatch.client.GiftSvcApi;
import com.kd7uiy.potlatch.repository.GiftChainRepository;
import com.kd7uiy.potlatch.repository.GiftImageRepository;
import com.kd7uiy.potlatch.repository.GiftRepository;
import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftChain;
import com.kd7uiy.potlatch.shared.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GiftChainController {
	@Autowired
	private GiftImageRepository mGiftImageRepository;

	@Autowired
	private GiftChainRepository mGiftChainRepository;
	
	@Autowired
	private GiftRepository mGiftRepository;

	@RequestMapping(value=GiftSvcApi.CHAIN_SVC_PATH,method=RequestMethod.GET)
	public @ResponseBody List<GiftChain> createChain() throws IOException {
		Iterable<GiftChain> chains=mGiftChainRepository.findAll();
		List<GiftChain> ret=new ArrayList<>();
		for (GiftChain chain: chains) {
			System.out.println(Arrays.toString(mGiftRepository.getCountByChainId(chain.getId())));
			chain.setGiftCount(mGiftRepository.getCountByChainId(chain.getId()));
			chain.setTouchedCount(mGiftRepository.getTouchCountByChainId(chain.getId()));
			chain.setFlagCount(mGiftRepository.getFlagCountByChainId(chain.getId()));
			ret.add(chain);
		}
		return ret;
	}
	
	@RequestMapping(value=GiftSvcApi.CHAIN_SVC_PATH+"/create",method=RequestMethod.POST)
	@PreAuthorize("hasRole('write')")
	public @ResponseBody GiftChain createChain(@RequestBody GiftChain chain, @AuthenticationPrincipal User user) throws IOException {
		chain.touchTime();
		chain.setDefaultImage(0);
		chain.setUserId(user.getId());
		chain=mGiftChainRepository.save(chain);
		System.out.println(user);
		return chain;
	}
	
	//TODO Eventually GiftChainPermissions permissions should be added to this, but for now, this is fine.
	@RequestMapping(value=GiftSvcApi.CHAIN_SVC_PATH+"/{id}/add",method=RequestMethod.POST)
	@PreAuthorize("hasRole('write')")
	public @ResponseBody boolean addGift(@PathVariable("id") long chainId, @RequestBody long giftId, @AuthenticationPrincipal User user) throws IOException {
		GiftChain chain=mGiftChainRepository.findOne(chainId);	
		Gift gift=mGiftRepository.findOne(giftId);
		System.out.println(gift);
		
		if (!((gift.getUserId()==user.getId())) && (user.getId()==chain.getUserId())) {
			throw new WrongUserFoundException("Only the gift owner can add an image to a chain");
		}
		gift.setChainId(chainId);
		if (chain.getDefaultImage()==0) {
			chain.setDefaultImage(giftId);
		}
		mGiftRepository.save(gift);
		return true;
	}
	
	@RequestMapping(value=GiftSvcApi.CHAIN_SVC_PATH+"/{id}",method=RequestMethod.GET)
	public @ResponseBody Collection<Gift> getGiftChain(@PathVariable("id") long chainId) {
		return mGiftRepository.findByChainId(chainId);
	}
	
	@RequestMapping(value=GiftSvcApi.CHAIN_SVC_PATH+"/{id}/get",method=RequestMethod.GET)
	public @ResponseBody byte[] downloadGift(@PathVariable("id") long id) throws IOException {
		GiftChain chain=mGiftChainRepository.findOne(id);
		return mGiftImageRepository.findOne(chain.getDefaultImage()).getImage();
	}
}
