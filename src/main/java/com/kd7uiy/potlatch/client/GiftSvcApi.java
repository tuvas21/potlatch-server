package com.kd7uiy.potlatch.client;
/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.Collection;

import com.kd7uiy.potlatch.shared.Gift;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * This provides the basic structures for managing Gifts
 */
public interface GiftSvcApi {

	public static final String TITLE_PARAMETER = "title";
	public static final String GIFT_CHAIN_ID_PARAM = "chainId";

	public static final String TOKEN_PATH = "/oauth/token";

	// The path where we expect the GiftSvc to live
	public static final String GIFT_SVC_PATH = "/gift";
	
	// The path where we expect the FlagSvc to live
	public static final String FLAG_SVC_PATH = "/flag";

	// The path to search videos by title
	public static final String GIFT_TITLE_SEARCH_PATH = GIFT_SVC_PATH + "/search/{title}";

	public static final String CHAIN_SVC_PATH = "/chain";
	
	@GET(GIFT_SVC_PATH+"/chain/{id}")
	public Collection<Gift> getGiftList(@Query("chainId") long id);
	
	@GET(GIFT_SVC_PATH + "/{id}")
	public Gift getGift(@Path("id") long id);
	
	@POST(GIFT_SVC_PATH)
	public Gift addGift(@Body Gift v);
	
	@POST(GIFT_SVC_PATH + "/{id}/touch")
	public Void touchGift(@Path("id") long id);
	
	@POST(GIFT_SVC_PATH + "/{id}/touch")
	public Void untouchGift(@Path("id") long id);
	
	@POST(GIFT_SVC_PATH + "/{id}/flag")
	public Void flagGift(@Path("id") long id);
	
	@POST(GIFT_SVC_PATH + "/{id}/glag")
	public Void unflagGift(@Path("id") long id);
	
	@GET(GIFT_TITLE_SEARCH_PATH)
	public Collection<Gift> findByTitle(@Query(TITLE_PARAMETER) String title);
	
	@GET(GIFT_SVC_PATH + "/{id}/touchedby")
	public Collection<String> getUsersWhoGiftTouched(@Path("id") long id);
	
	//Most touching images
	@GET(GIFT_SVC_PATH + "/touching")
	public Collection<Gift> getMostTouchingGift();
}
