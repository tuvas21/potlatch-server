package com.kd7uiy.potlatch.shared;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Objects;

/** Significantly modified code from the Android Cloud Computing course
 * 
 * @author original- mitchell
 * @author modified- TBD
 */
@Entity
public class Gift {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String title;

	private long userId;
	private long chainId;
	private String description;
	private long time;
	
	private long touchCount;
	private long flagCount;
	
//	@JsonIgnore
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable (name="gift_touched_users", joinColumns=@JoinColumn(name="user_id"))
	@OrderColumn
	List<GiftVote> touchedUsers;
	
//	@JsonIgnore
	@ElementCollection(fetch=FetchType.LAZY)
	List<GiftVote> flaggedUsers;
	
	
	public Gift() {
	}
	
    public List<GiftVote> getTouchedUsers() {
        return touchedUsers;
    }

    public void setTouchedUsers(List<GiftVote> touchedUsers) {
        this.touchedUsers = touchedUsers;
    }
    
    public List<GiftVote> getFlaggedUsers() {
        return flaggedUsers;
    }

    public void setFlaggedUsers(List<GiftVote> flaggedUsers) {
        this.flaggedUsers = flaggedUsers;
    }

	public Gift(String title, long user, long chain, String description) {
		super();
		this.title = title;
		this.userId=user;
		this.chainId=chain;
		this.description=description;
	}
	
	public long getTouchCount() {
		return touchCount;
	}

	public void setTouchCount(long touchCount) {
		this.touchCount = touchCount;
	}
	
	public long getFlagCount() {
		return flagCount;
	}

	public void setFlagCount(long flagCount) {
		this.flagCount = flagCount;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getChainId() {
		return chainId;
	}
	
	public void setChainId(long chainId) {
		this.chainId = chainId;
	}
	
	public long getTime() {
		return time;
	}
	
	public void setTime(long time) {
		this.time=time;
	}
	
	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, url, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(title, description, time);
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Gift) {
			Gift other = (Gift) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(title, other.title)
					&& Objects.equal(chainId, other.chainId)
					&& Objects.equal(userId, other.userId)
					&& description.equals(other.description);
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("Gift object \n");
		sb.append("Id = " + id + "\n");
		sb.append("Title="+title+"\n");
		sb.append("Description="+description+"\n");
		sb.append("User="+userId+"\n");
		for (GiftVote vote: touchedUsers) {
			sb.append("Touched User="+vote.getUserId()+"\n");
		}
		for (GiftVote vote: flaggedUsers) {
			sb.append("Flagged User="+vote.getUserId()+"\n");
		}
		sb.append("Chain ID="+chainId);
		return sb.toString();
	}

}
