package com.kd7uiy.potlatch.shared;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

@Entity
public class User implements UserDetails{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4207004756397132393L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String name;
	@Column(unique=true)
	private String openId;
	private String description;
	
	@Transient
	private int giftCount;
	@Transient
	private int touchCount;
	@Transient
	private int flagCount;
	
	
	public int getGiftCount() {
		return giftCount;
	}

	@JsonProperty("giftCount")
	public void setGiftCount(int giftCount) {
		this.giftCount = giftCount;
	}

	public int getTouchCount() {
		return touchCount;
	}

	@JsonProperty("touchCount")
	public void setTouchCount(int touchCount) {
		this.touchCount = touchCount;
	}

	public int getFlagCount() {
		return flagCount;
	}

	@JsonProperty("flagCount")
	public void setFlagCount(int flagCount) {
		this.flagCount = flagCount;
	}

//	@JsonProperty("filterOffensive")
	private boolean filterOffensive;
//	@JsonProperty("pushNotifications")
	private boolean pushNotifications;

	//Don't send out the password!
	
	private String password;
	
	public static User create(String username, String password, boolean filterOffensive) {
		User user=new User();
		user.openId=username;
		user.password=password;
		user.filterOffensive=filterOffensive;
		user.description="";
		user.pushNotifications=true;
		user.name="";
		return user;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}
	
	@JsonIgnore
	public boolean getFilterOffensive() {
		return filterOffensive;
	}
	@JsonProperty("filterOffensive")
	public void setFilterOffensive(boolean filterOffensive) {
		this.filterOffensive=filterOffensive;
	}
	
	@JsonIgnore
	public boolean getPushNotifications() {
		return pushNotifications;
	}
	@JsonProperty("pushNotifications")
	public void setPushNotifications(boolean pushNotifications) {
		this.pushNotifications=pushNotifications;
	}

	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return password;
	}
	
	@JsonProperty("password")
	public void setPassword(String password) {
		this.password=password;
	}

	@Override
	public String getUsername() {
		return openId;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return true;
	}
	
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(description, openId, name);
	}

	@Override
	public boolean equals(Object obj) {
//		System.out.println("In Equals Statement");
		if (obj instanceof User) {
			User other = (User) obj;
			// Google Guava provides great utilities for equals too!
			System.out.println("id:"+id+","+other.id);
			System.out.println("openId:"+openId+","+other.openId);
			System.out.println("name:"+name+","+other.name);
			System.out.println("description:"+description+","+other.description);
			return Objects.equal(id, other.id)
					&& Objects.equal(openId, other.openId)
					&& Objects.equal(name, other.name)
					&&  Objects.equal(description,other.description);
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("id="+id+"\n");
		sb.append("name="+name+"\n");
		sb.append("id="+id+"\n");
		sb.append("openId="+openId+"\n");
		sb.append("description="+description+"\n");
		sb.append("filterOffensive="+filterOffensive+"\n");
		sb.append("pushNotifications="+pushNotifications+"\n");
		sb.append("password="+password+"\n");
		return sb.toString();
	}
	
	@JsonIgnore
	public void setGiftCount(Integer[] count) {
		if (count==null || count.length==0 || count[0]==null) {
			setGiftCount(0);
		} else {
			setGiftCount(count[0]);
		}
	}

	@JsonIgnore
	public void setFlagCount(Integer[] count) {
		if (count==null || count.length==0 || count[0]==null) {
			setFlagCount(0);
		} else {
			setFlagCount(count[0]);
		}
	}
	
	@JsonIgnore
	public void setTouchCount(Integer[] count) {
		if (count==null || count.length==0 || count[0]==null) {
			setTouchCount(0);
		} else {
			setTouchCount(count[0]);
		}
	}

}
