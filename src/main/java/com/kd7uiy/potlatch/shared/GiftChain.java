package com.kd7uiy.potlatch.shared;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class GiftChain {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String name;
	private long defaultImage;
	private String description;
	private long userId;
	@JsonIgnore
	private long time;
	
	@Transient
	private int giftCount;
	@Transient
	private int touchedCount;
	@Transient
	private int flagCount;
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getGiftCount() {
		return giftCount;
	}

	@JsonProperty("giftCount")
	public void setGiftCount(int giftCount) {
		this.giftCount = giftCount;
	}

	public int getTouchedCount() {
		return touchedCount;
	}

	@JsonProperty("touchedCount")
	public void setTouchedCount(int touchedCount) {
		this.touchedCount = touchedCount;
	}

	public int getFlagCount() {
		return flagCount;
	}

	@JsonProperty("flagCount")
	public void setFlagCount(int flagCount) {
		this.flagCount = flagCount;
	}

	public GiftChain() {
		touchTime();
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getDefaultImage() {
		return defaultImage;
	}
	
	public void setDefaultImage(long defaultImage) {
		this.defaultImage = defaultImage;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public long getTime() {
		return time;
	}

	public void touchTime() {
		time=System.currentTimeMillis();
	}
	
	@JsonIgnore
	public void setGiftCount(Integer[] count) {
		if (count==null || count.length==0 || count[0]==null) {
			setGiftCount(0);
		} else {
			setGiftCount(count[0]);
		}
	}

	@JsonIgnore
	public void setFlagCount(Integer[] count) {
		if (count==null || count.length==0 || count[0]==null) {
			setFlagCount(0);
		} else {
			setFlagCount(count[0]);
		}
	}
	
	@JsonIgnore
	public void setTouchedCount(Integer[] count) {
		if (count==null || count.length==0 || count[0]==null) {
			setTouchedCount(0);
		} else {
			setTouchedCount(count[0]);
		}
	}
	
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("id="+id+"\n");
		sb.append("name="+name+"\n");
		sb.append("description="+description+"\n");
		sb.append("time="+time+"\n");
		sb.append("image="+defaultImage+"\n");
		return sb.toString();
	}

}
