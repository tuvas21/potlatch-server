package com.kd7uiy.potlatch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kd7uiy.potlatch.shared.GiftImage;

public interface GiftImageRepository  extends JpaRepository<GiftImage, Long>{

}
