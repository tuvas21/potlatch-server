package com.kd7uiy.potlatch.repository;

import java.util.Collection;
import java.util.List;

import com.kd7uiy.potlatch.client.GiftSvcApi;
import com.kd7uiy.potlatch.shared.Gift;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

@RepositoryRestResource(path = GiftSvcApi.GIFT_SVC_PATH)
public interface GiftRepository extends CrudRepository<Gift, Long>{

	// Find all videos with a matching title (e.g., Video.name)
	public Collection<Gift> findByTitle(
			// The @Param annotation tells Spring Data Rest which HTTP request
			// parameter it should use to fill in the "title" variable used to
			// search for Videos
			@Param(GiftSvcApi.TITLE_PARAMETER) String title);

	public Collection<Gift> findByChainId(@Param(GiftSvcApi.GIFT_CHAIN_ID_PARAM) long chainId);
	public List<Gift> findByUserId(@Param("userId") long id);
	public Collection<Gift> findAll(Sort sort);

	public Collection<Gift> findByTitleContaining(String title);

	@Query("select count(g),sum(g.touchCount) FROM Gift g WHERE g.chainId=?1")
	public Integer[] getCountByChainId(long chainId);
	
	@Query("select sum(g.touchCount) FROM Gift g WHERE g.chainId=?1")
	public Integer[] getTouchCountByChainId(long chainId);

	@Query("select sum(g.flagCount) FROM Gift g WHERE g.chainId=?1")
	public Integer[] getFlagCountByChainId(long id);
	
	@Query("select count(g),sum(g.touchCount) FROM Gift g WHERE g.userId=?1")
	public Integer[] getCountByUserId(long chainId);
	
	@Query("select sum(g.touchCount) FROM Gift g WHERE g.userId=?1")
	public Integer[] getTouchCountByUserId(long chainId);

	@Query("select sum(g.flagCount) FROM Gift g WHERE g.userId=?1")
	public Integer[] getFlagCountByUserId(long id);
	
	/*
	 * See: http://docs.spring.io/spring-data/jpa/docs/1.3.0.RELEASE/reference/html/jpa.repositories.html 
	 * for more examples of writing query methods
	 */
	
}
